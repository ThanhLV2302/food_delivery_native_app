import React from 'react';
import {Navigation} from './src/navigation';

function App(): JSX.Element {
  return (
    <>
      <Navigation></Navigation>
    </>
  );
}

export default App;
